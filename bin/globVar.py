#!/usr/bin/env python
import os

# MAIN DIR
# /home/nmsadm/cadev/cell_status
BASE_DIR = os.path.split(os.path.realpath(__file__))[0]

# Directory
CONF_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "conf"))
LOG_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "log"))
OUTPUT_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "output"))
TEMP_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "temp"))

# Files
LOG_FILE = os.path.abspath(os.path.join(LOG_DIR, "cell_status.log"))
LOGGER_FILE = os.path.abspath(os.path.join(CONF_DIR, "logger.conf"))

if __name__ == "__main__":
    print BASE_DIR
    print LOGGER_FILE