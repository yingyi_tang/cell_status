import json
import globVar
import logging
import logging.config
import os
import time
import subprocess
import ConfigParser

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("cell")
except Exception, e:
    print(e)

TIME = time.strftime("%Y%m%d%H%M", time.localtime())
CELL_STATUS_FILE = globVar.OUTPUT_DIR + os.path.sep + "cell_status_" + TIME + ".json"
CELL_STATUS_TEMP_FILE = globVar.TEMP_DIR + os.path.sep + "cell_status.tmp"
CONFIG_INI_FILE = globVar.CONF_DIR + os.path.sep + "cell_status.ini"

def export_cell_status():
    logger.info("export start")
    gen_tdd_cell_status_cmd = "/opt/ericsson/nms_cif_cs/etc/unsupported/bin/cstest -s Seg_masterservice_CS lt EUtranCellTDD -an administrativeState operationalState > {file}".format(file=CELL_STATUS_TEMP_FILE)
    ret = subprocess.call(gen_tdd_cell_status_cmd, shell=True)
    if ret==0:
        logger.info("run {cmd}".format(cmd=gen_tdd_cell_status_cmd))
    else:
        logger.info("run {cmd} failed".format(cmd=gen_tdd_cell_status_cmd))
        exit(1)

    gen_fdd_cell_status_cmd = "/opt/ericsson/nms_cif_cs/etc/unsupported/bin/cstest -s Seg_masterservice_CS lt EUtranCellFDD -an administrativeState operationalState >> {file}".format(file=CELL_STATUS_TEMP_FILE)
    ret = subprocess.call(gen_fdd_cell_status_cmd, shell=True)
    if ret==0:
        logger.info("run {cmd}".format(cmd=gen_fdd_cell_status_cmd))
    else:
        logger.info("run {cmd} failed".format(cmd=gen_fdd_cell_status_cmd))
        exit(1)
    gen_nb_cell_status_cmd = "/opt/ericsson/nms_cif_cs/etc/unsupported/bin/cstest -s Seg_masterservice_CS lt NbIotCell -an administrativeState operationalState >> {file}".format(file=CELL_STATUS_TEMP_FILE)
    ret = subprocess.call(gen_nb_cell_status_cmd, shell=True)
    if ret==0:
        logger.info("run {cmd}".format(cmd=gen_nb_cell_status_cmd))
    else:
        logger.info("run {cmd} failed".format(cmd=gen_nb_cell_status_cmd))
        exit(1)
    logger.info("generated {file}".format(file=CELL_STATUS_TEMP_FILE))

    # Generate cell_status
    statusDict = {}
    cell = ads = ops = None
    cellNum = 0
    with open(CELL_STATUS_TEMP_FILE, "r") as f:
        lines = f.readlines()
    logger.info("read {file}".format(file=CELL_STATUS_TEMP_FILE))
    for line in lines:
        if line.lstrip().startswith("SubNetwork=ONRM_ROOT_MO_R"):
            cell = line.lstrip().rstrip("\n")
        elif line.lstrip().startswith("[1] administrativeState"):
            ads = line.split(":")[1].lstrip().rstrip("\n")
        elif line.lstrip().startswith("[2] operationalState"):
            ops = line.split(":")[1].lstrip().rstrip("\n")
        else:
            pass
        if cell and ads and ops:
            cellNum += 1
            statusDict[cell] = [{"administrativeState": ads, "operationalState": ops}]
            cell = ads = ops = None
    logger.info("found total cell number: {cellNum}".format(cellNum=cellNum))

    with open(CELL_STATUS_FILE, "w") as f1:
        json.dump(statusDict, f1, indent=1, sort_keys=True)
    logger.info("generated {file}".format(file=CELL_STATUS_FILE))
    logging.info("export end")

def house_keeping():
    conf = ConfigParser.ConfigParser()
    try:
        conf.read(CONFIG_INI_FILE)
        outputRetentionDays = conf.get("housekeeping", "output_retention_days")
        cmd = "/bin/find {outputDir}/ -mtime {outputRetentionDays} -type f -exec rm {char} \;".format(outputDir=globVar.OUTPUT_DIR, outputRetentionDays=outputRetentionDays,char="{}")
        ret = subprocess.call(cmd, shell=True)
        if ret==0:
            logger.info("run {cmd}".format(cmd=cmd))
        else:
            logger.info("run {cmd} failed".format(cmd=cmd))

        # M
        minSize = int(conf.get("housekeeping", "log_size"))
        fsize = os.path.getsize(globVar.LOG_FILE)/1024/2014
        logger.info("{file} size: {size}M, minimun size: {msize}M".format(file=globVar.LOG_FILE, size=fsize, msize=minSize))
        if fsize >= minSize:
            os.remove(globVar.LOG_FILE)
            logger.info("remove {file}".format(file=globVar.LOG_FILE))
    except Exception, e:
        logger.error(e)

if __name__ == "__main__":
    export_cell_status()
    house_keeping()
